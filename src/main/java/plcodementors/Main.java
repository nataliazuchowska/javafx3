package plcodementors;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    public static void main(String args) {
        launch(args);
    }


    @Override
    public void start(Stage stage) throws Exception {



        VBox left = new VBox();
        List<Control> leftControls = new ArrayList<>();
        leftControls.add(new Button("open"));
        leftControls.add(new Button("save"));

        left.getChildren().addAll(leftControls);

        leftControls.forEach(c -> c.setMaxHeight(Double.POSITIVE_INFINITY));
        leftControls.forEach(c -> VBox.setVgrow(c, Priority.ALWAYS));

        TextArea center = new TextArea();

        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(left);
        borderPane.setCenter(center);

        ((Button) leftControls.get(0)).setOnAction(event -> openFile(event, center));
        ((Button) leftControls.get(0)).setOnAction(event -> saveFile(event, center));
        Scene scene = new Scene(borderPane, 600, 600);
        scene.getStylesheets().add("/tmp/przyciski");
        stage.setScene(scene);
        stage.show();
    }

    public void openFile(ActionEvent event, TextArea center) {
        FileChooser openFile = new FileChooser();
        File file = openFile.showOpenDialog(null);
        center = new TextArea();

        try {
            FileReader fr = new FileReader(file);
            BufferedReader bf = new BufferedReader(fr);
            String s = bf.readLine();
            String zawartosc = null;

            while (s != null) {
                zawartosc = s;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }


    }

    public void saveFile(ActionEvent event, TextArea center) {
        FileChooser saveFile = new FileChooser();
        File file = saveFile.showSaveDialog(null);
        center = new TextArea();

        FileWriter fw = null;
        try {
            fw = new FileWriter(file);
            fw.write(String.valueOf(center));
            fw.write("/n");

            file.setExecutable(false);
            fw.close();

        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }
}